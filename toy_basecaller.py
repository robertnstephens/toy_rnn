#! /usr/bin/env python3

# http://machinelearningmastery.com/time-series-prediction-lstm-recurrent-neural-networks-python-keras/
# Video: https://www.youtube.com/watch?v=ftMq5ps503w

import sys
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
from sklearn.preprocessing import MinMaxScaler
import os

# brew install graphviz
# pip3 install graphviz
# pip3 install pydot-ng
from keras.utils.vis_utils import plot_model

import matplotlib.pyplot as plt


#timesteps = seq_length = 1
data_dim = 3

xy = np.loadtxt(sys.argv[1], delimiter=',')
print(len(xy[0]))


# very important. It does not work without it.
scaler = MinMaxScaler(feature_range=(0, 1))
xy = scaler.fit_transform(xy)

num_classes = pow(4, 3)

X = xy[:, 0:-num_classes-1] 
Y = xy[:, -num_classes-1:-1]

data_size = len(X) # num rows

seq_length = len(X[0])
X = X.reshape(data_size, 1, seq_length)


# split to train and testing
train_size = int(len(Y) * 0.7)
test_size = len(Y) - train_size
trainX, testX = np.array(X[0:train_size]), np.array(X[train_size:len(X)])
trainY, testY = np.array(Y[0:train_size]), np.array(Y[train_size:len(Y)])

model = Sequential()
model.add(LSTM(200, input_shape=(1, seq_length), return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(200, return_sequences=False))
model.add(Dense(num_classes, use_bias=True)) #dim of final layer has to equal num_cols of Y
model.add(Activation('softmax'))

model.summary()

# Store model graph in png
# (Error occurs on in python interactive shell)
plot_model(model, to_file=os.path.basename(__file__) + '.png', show_shapes=True)

model.compile(loss='categorical_crossentropy',
              optimizer='adam')

model.fit(trainX, trainY, epochs=200)

# make predictions
testPredict = model.predict(testX)

mi = np.argmax(testPredict,1)

for i in mi:
    print(i)

plt.plot(testY)
plt.plot(testPredict)
plt.show()

