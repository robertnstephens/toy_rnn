#! /usr/bin/env python3

import sys, time
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import os

# brew install graphviz
# pip3 install graphviz
# pip3 install pydot-ng

import mxnet as mx
import matplotlib.pyplot as plt


sys.path.append("./py/")
import Wraph5py


def viterbi_backtrace(post):
    
    # posterior matrix, each row of which sums to 1.0

    lpost = post.copy() #TODO

    state_seq = np.zeros(len(post), dtype=int)
    #state_seq[-1] = np.argmax(pscore)
    state_seq[-1] = np.argmax(post[-1])
    
    for ev in range(len(post), 1, -1):
        # Viterbi backtrace
        state_seq[ev-2] = int(lpost[ev-2][state_seq[ev-1]])

    #return np.amax(pscore), state_seq
    return state_seq
    
    
    
if __name__ == "__main__":
    
    #timesteps = seq_length = 1
    #batch_size = 2
    batch_size = 12
    #TODO
    num_classes = pow(4, 3)


    path = sys.argv[1]
    files = os.listdir(path)
    f = path + files[0]

    print("files:")
    for seqf in files:
        print(seqf)

    #xy, y, labels = Wraph5py.get_training_data(f)
    X, Y_num, labels = Wraph5py.get_training_data(f)

    print( type(X), type(Y_num), type(labels) )

    Y = []
    for p in Y_num:
        row = [0 for _ in range(num_classes)]
        row[p-1] = 1
        Y.append(row)

    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(0, 1))
    X = scaler.fit_transform(X)
    Y = scaler.fit_transform(Y)

    #X = xy[:, 0:-num_classes-1] 
    #Y = xy[:, -num_classes-1:-1]

    data_size = len(X) # num rows

    seq_length = len(X[0])
    X = X.reshape(data_size, 1, seq_length) #TODO


    ''' 
    xy = np.loadtxt(sys.argv[1], delimiter=',')
    print(len(xy[0]))
    
    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(0, 1))
    xy = scaler.fit_transform(xy)
    
    X = xy[:, 0:-num_classes-1] 
    Y = xy[:, -num_classes-1:-1]
    
    data_size = len(X) # num rows
    
    seq_length = len(X[0])
    X = X.reshape(data_size, 1, seq_length) #TODO

    ''' 
    
    print("X shape:", X.shape)
    print("Y shape:", Y.shape)
    
    # split to train and testing
    train_size = int(len(Y) * 0.7)
    test_size = len(Y) - train_size
    trainX, testX = np.array(X[0:train_size]), np.array(X[train_size:len(X)])
    trainY, testY = np.array(Y[0:train_size]), np.array(Y[train_size:len(Y)])
    
    '''
    model = Sequential()
    model.add(LSTM(200, input_shape=(1, seq_length), return_sequences=True))
    model.add(Dropout(0.1))
    model.add(LSTM(200, return_sequences=False))
    model.add(Dense(num_classes, use_bias=True)) #dim of final layer has to equal num_cols of Y
    model.add(Activation('softmax'))
    '''
    
    def build_sym(seq_len):
        data = mx.sym.var("data")  # Shape: (N, T, C)
        target = mx.sym.var("target")  # Shape: (N, T, C)
      
        blstm_0 = 128
        feedforward_tanh_1 = 64
        blstm_2 = 128
        feedforward_tanh_3 = 64
        softmax_4 = 1025 #TODO len(kmer)+1 -> num_classes

        lstm1 = mx.rnn.LSTMCell(num_hidden=blstm_0, prefix="lstm1_")
        lstm2 = mx.rnn.LSTMCell(num_hidden=blstm_2, prefix="lstm2_")
        
        L1, _ = lstm1.unroll(length=seq_len, inputs=data, merge_outputs=True, layout="TNC")  # Shape: (T, N, 5)
        L1 = mx.sym.Dropout(L1, p=0.1)  # Shape: (T, N, 5)

        fc1 = mx.sym.FullyConnected(L1, name='fc1', num_hidden=feedforward_tanh_1)
        L2, L2_states = lstm2.unroll(length=seq_len, inputs=fc1, merge_outputs=True, layout="TNC")  # Shape: (T, N, 10)
        fc2 = mx.sym.FullyConnected(L2, name='fc2', num_hidden=feedforward_tanh_3)
        

        #pred = mx.sym.FullyConnected(fc2, num_hidden=num_classes, name="pred")
        #pred = mx.sym.LinearRegressionOutput(data=pred, label=target)

        # reshape to batch_size, num_output, num_class????
        softmax = mx.symbol.SoftmaxOutput(data=fc2, multi_output=True, label=target, name='sm')
        #pred = mx.sym.LinearRegressionOutput(data=softmax, label=target)

        return softmax
    
    
    # infer_shape error. Arguments:
    # data: (1, 1, 2)
    # target: (1, 64)
    
    
    def train_eval_net():
        pred = build_sym(seq_len=seq_length)
    
        net = mx.mod.Module(symbol=pred, data_names=['data'], label_names=['target'], context=mx.cpu())
    
        train_iter = mx.io.NDArrayIter(data=trainX, label=trainY,
                                       data_name="data", label_name="target",
                                       batch_size=batch_size,
                                       shuffle=True)
        test_iter = mx.io.NDArrayIter(data=testX, label=testY,
                                      data_name="data", label_name="target",
                                      batch_size=batch_size)
        net.fit(train_data=train_iter, eval_data=test_iter,
                initializer=mx.init.Xavier(rnd_type="gaussian", magnitude=1),
                optimizer="adam",
                optimizer_params={"learning_rate": 1E-3},
                eval_metric="mse", num_epoch=10)
    
        # make predictions
        testPredict = net.predict(test_iter).asnumpy()
        mse = np.mean((testPredict - testY)**2)
        return testPredict, mse
    
    
    
    
    print("Begin to train LSTM without CUDNN acceleration...")
    begin = time.time()
    normal_pred, normal_mse = train_eval_net()
    end = time.time()
    normal_time_spent = end - begin
    print("Done!")
    
    #rs_mod
    #print("CUDNN time spent: %g, test mse: %g" % (cudnn_time_spent, cudnn_mse))
    print("NoCUDNN time spent: %g, test mse: %g" % (normal_time_spent, normal_mse))
    
    plt.close('all')
    fig = plt.figure()
    plt.plot(testY, label='Groud Truth')
    #plt.plot(cudnn_pred, label='With cuDNN')
    plt.plot(normal_pred, label='Without cuDNN')
    plt.legend()
    plt.show()
  

    print("#"*40)
    print("row0")
    print( sum(normal_pred[0]) )
    print( sum(normal_pred[10]) )
    print(normal_pred[0])
    print("#"*40)

    for r in normal_pred:
        #print(int(i*64))
        #print(r)
        print(np.argmax(r))
    
    res = viterbi_backtrace(normal_pred)
   
    print(res)
 
