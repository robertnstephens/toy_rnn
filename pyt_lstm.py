#! /usr/bin/env python3

import sys
# Lab 6 Softmax Classifier
import torch
from torch.autograd import Variable
import numpy as np
from sklearn.preprocessing import MinMaxScaler

if __name__ == '__main__':
    torch.manual_seed(666)  # for reproducibility
    
    xy = np.loadtxt(sys.argv[1], delimiter=',', dtype=np.float32)

    features = 2
    num_classes = pow(4, 3)
    #x_data = xy[:, 0:-num_classes-1]
    x_data = xy[:, 0:features]
    y_data = xy[:, [-1]]
    y_data = y_data.astype(np.int64)

    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(-1, 1))
    x_data = scaler.fit_transform(x_data)
    
    hidden_size = 50
    time_steps = 1

    print("x_data.shape, y_data.shape")
    print(x_data.shape, y_data.shape)
    
    data_len = x_data.shape[0]
    features = x_data.shape[1]
    print("data_len:", data_len, "features:", features)
    
    train_ratio = 0.7
    t_end = int(len(x_data)*train_ratio)

    trainX = x_data[:t_end]
    trainY = y_data[:t_end]
    valX = x_data[t_end:]
    valY = y_data[t_end:]

    # py_torch ordering (time_steps, batch_size, features)
    trainX = trainX.reshape(time_steps, -1, features)
    valX = valX.reshape(time_steps, -1, features)
    trainY = trainY.reshape(-1,) 
    valY = valY.reshape(-1,)

    print("shapes:")
    print(trainX.shape, trainY.shape, valX.shape, valY.shape)

    trainX = Variable(torch.from_numpy(trainX))
    trainY = Variable(torch.from_numpy(trainY))
    valX = Variable(torch.from_numpy(valX))
    valY = Variable(torch.from_numpy(valY))

    model = torch.nn.LSTM(input_size=features, 
                          hidden_size=num_classes, 
                          num_layers=2)

    softmax = torch.nn.Softmax()
    
    loss = torch.nn.CrossEntropyLoss() # loss AKA criterion
    optimizer = torch.optim.SGD(model.parameters(), lr=0.2)
   
    num_epochs = 100000
 
    for epoch in range(num_epochs):
        model.zero_grad() 
        
        output_seq, _ = model(trainX)
       
        last_output = output_seq[-1]

        # (700 x 64), (700, )
        err = loss(last_output, trainY) # err AKA cost
        err.backward()
        optimizer.step()
    
        prediction = torch.max(softmax(last_output), 1)[1]
        correct_prediction = (prediction.data == trainY.data)
        accuracy = correct_prediction.float().mean()
   
        if epoch % 50 == 0:
            print("epoch: {:5}\tloss: {:.3f}\tacc: {:.2}".format(epoch, err.data[0], accuracy))
            
            output_seq, _ = model(valX)
            last_output = output_seq[-1]
            prediction = torch.max(softmax(last_output), 1)[1]
            correct_prediction = (prediction.data == valY.data)
            accuracy = correct_prediction.float().mean()
            print("val_acc: {:.2}".format(accuracy))
            sys.stdout.flush()


    output_seq, _ = model(valX)
    last_output = output_seq[-1]
    prediction = torch.max(softmax(last_output), 1)[1]
    correct_prediction = (prediction.data == valY.data)
    accuracy = correct_prediction.float().mean()
    print("final val_acc: {:.2}".format(accuracy))

