#! /usr/bin/env python3

# http://machinelearningmastery.com/time-series-prediction-lstm-recurrent-neural-networks-python-keras/
# Video: https://www.youtube.com/watch?v=ftMq5ps503w

import sys, time
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import os

# brew install graphviz
# pip3 install graphviz
# pip3 install pydot-ng

import mxnet as mx
import matplotlib.pyplot as plt

xy = np.loadtxt(sys.argv[1], delimiter=',')
print(len(xy[0]))

# very important. It does not work without it.
scaler = MinMaxScaler(feature_range=(0, 1))
xy = scaler.fit_transform(xy)

#TODO
# num_classes = pow(4, 3)
num_classes = 1

X = xy[:, 0:-num_classes] 
Y = xy[:, -num_classes:]

print("xys:", xy[0], X[0], Y[0])

batch_size = 1

data_size = len(X) # num rows
seq_length = 1
data_dim = len(X[0])

X = X.reshape(data_size, seq_length, data_dim)

# split to train and testing
train_size = int(len(Y) * 0.7)
test_size = len(Y) - train_size
trainX, testX = np.array(X[0:train_size]), np.array(X[train_size:len(X)])
trainY, testY = np.array(Y[0:train_size]), np.array(Y[train_size:len(Y)])

print("trainX.shape", trainX.shape)

'''
model = Sequential()
model.add(LSTM(200, input_shape=(1, seq_length), return_sequences=True))
model.add(Dropout(0.1))
model.add(LSTM(200, return_sequences=False))
model.add(Dense(num_classes, use_bias=True)) #dim of final layer has to equal num_cols of Y
model.add(Activation('softmax'))
'''

def build_sym(seq_len):
   
    print("seq_len", seq_len)
    
    data = mx.sym.var("data")  # Shape: (N, T, C)
    target = mx.sym.var("target")  # Shape: (N, T, C)
    #data = mx.sym.transpose(data, axes=(1, 0, 2))  # Shape: (T, N, C)
    lstm1 = mx.rnn.LSTMCell(num_hidden=200, prefix="lstm1_")
    lstm2 = mx.rnn.LSTMCell(num_hidden=200, prefix="lstm2_")
    L1, _ = lstm1.unroll(length=seq_len, inputs=data, merge_outputs=True, layout="TNC")  # Shape: (T, N, 5)
    L1 = mx.sym.Dropout(L1, p=0.1)  # Shape: (T, N, 5)
    
    L2, L2_states = lstm2.unroll(length=seq_len, inputs=L1, merge_outputs=True, layout="TNC")  # Shape: (T, N, 10)
    
    #_, L2_states = lstm2.unroll(length=seq_len, inputs=L1, merge_outputs=True, layout="TNC")  # Shape: (T, N, 10)
    #L2 = mx.sym.reshape(L2_states[0], shape=(-1, 0), reverse=True)  # Shape: (T * N, 10)
    pred = mx.sym.FullyConnected(L2, num_hidden=num_classes, name="pred")
    pred = mx.sym.LinearRegressionOutput(data=pred, label=target)
    return pred


def train_eval_net():
    pred = build_sym(seq_len=seq_length)

    net = mx.mod.Module(symbol=pred, data_names=['data'], label_names=['target'], context=mx.cpu())

    train_iter = mx.io.NDArrayIter(data=trainX, label=trainY,
                                   data_name="data", label_name="target",
                                   batch_size=batch_size,
                                   shuffle=True)
    test_iter = mx.io.NDArrayIter(data=testX, label=testY,
                                  data_name="data", label_name="target",
                                  batch_size=batch_size)
    net.fit(train_data=train_iter, eval_data=test_iter,
            initializer=mx.init.Xavier(rnd_type="gaussian", magnitude=1),
            optimizer="adam",
            optimizer_params={"learning_rate": 1E-3},
            eval_metric="mse", num_epoch=50)

    # make predictions
    testPredict = net.predict(test_iter).asnumpy()
    mse = np.mean((testPredict - testY)**2)
    return testPredict, mse


print("Begin to train LSTM without CUDNN acceleration...")
begin = time.time()
normal_pred, normal_mse = train_eval_net()
end = time.time()
normal_time_spent = end - begin
print("Done!")

#rs_mod
#print("CUDNN time spent: %g, test mse: %g" % (cudnn_time_spent, cudnn_mse))
print("NoCUDNN time spent: %g, test mse: %g" % (normal_time_spent, normal_mse))

plt.close('all')
fig = plt.figure()
plt.plot(testY, label='Groud Truth')
#plt.plot(cudnn_pred, label='With cuDNN')
plt.plot(normal_pred, label='Without cuDNN')
plt.legend()
plt.show()

for i in normal_pred:
    print(int(i*64))

