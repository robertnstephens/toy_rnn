#! /usr/bin/env python3

import sys
import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.utils import np_utils

X, Y = np.array([]), np.array([])
data_size = 1000
train_size = int(data_size*0.8)
seq_length = 8

num_classes = pow(2,seq_length)

for i in range(0, data_size):
   
    val1 = np.random.randint(0, num_classes)
    val2 = val1 >> 1 
    s1 = str(bin(val1))[2:]

    s1 = "0" * (seq_length-len(s1)) + s1

    x_row = [int(i) for i in s1]

    y_row = [0.0 for _ in range(num_classes)]
    y_row[val2] = 1.0

    if i == 0:
        X = np.array(x_row)
        Y = np.array(y_row)
    else:
        X = np.vstack((X, x_row))
        Y = np.vstack((Y, y_row))

X = X.reshape(data_size, 1, seq_length)

X_train, Y_train = X[:train_size], Y[:train_size]
X_val, Y_val = X[train_size:], Y[train_size:]


print(X_train.shape, Y_train.shape)

model = Sequential()
model.add(LSTM(32, input_shape=(1, seq_length), return_sequences=True))
model.add(LSTM(32))
model.add(Dense(num_classes, activation="softmax", kernel_initializer="normal"))

if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
    print("loading model...") 
    #del model  # deletes the existing model

    # returns a compiled model
    # identical to the previous one
    model = load_model('bin_model.h5')
else:
    print("training and saving model...") 
    
    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])
    
    #model.compile(loss='mean_absolute_error', optimizer='rmsprop', metrics=['accuracy'])
    model.fit(X_train, Y_train, 
              epochs=250, verbose=0,
              batch_size=64,
              validation_data=(X_val, Y_val))


    model.save('bin_model.h5')  # creates a HDF5 file 


scores = model.evaluate(X_val, Y_val, verbose=0)
print("Model accuracy: %.2f%%" % (scores[1]*100))
print("Model loss: %.2f%%" % (scores[0]*100))

import matplotlib.pyplot as plt
#%matplotlib inline

for r in range(5):
    t = np.array([X_val[r]])
    
    p = model.predict(t)
    i = np.argmax(p)
    s = str(bin(i))[2:]
    s = "0" * (seq_length-len(s)) + s
    ans = np.array([[int(c) for c in s]])
    
    res = np.array([0.0 for _ in range(seq_length)])
    for i, v in enumerate(p[0]):
        s = str(bin(i))[2:]
        s = "0"*(seq_length-len(s)) + s
        
        inds = []
        for j, w in enumerate(s):
            if w == "1":
                inds.append(j)
    
        res[inds] += 1.0*v
    
    res = [res]
    
    plt.imshow(np.concatenate( (t[0], ans, res) ), interpolation='nearest', cmap=plt.cm.Greys)
    plt.show()
    
    
    
